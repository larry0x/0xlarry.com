import { Store } from "vuex";
import { ConnectData, ProjectData, QRCodeData } from "@/types";

declare module "@vue/runtime-core" {
  interface State {
    connects: Array<ConnectData>;
    projects: Array<ProjectData>;
    qrCodes: Array<QRCodeData>;
  }

  interface ComponentCustomProperties {
    $store: Store<State>;
  }
}
