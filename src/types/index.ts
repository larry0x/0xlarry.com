export interface ProjectData {
  icon: string;
  name: string;
  url: string;
  enabled: boolean;
  description: string;
}

export interface ConnectData {
  icon: string;
  name: string;
  url: string;
}

export interface QRCodeData {
  name: string;
  img: string;
  address: string;
  blockExplorerName: string;
  blockExplorerUrlPrefix: string;
}
