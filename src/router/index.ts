import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";
import Donations from "../views/Donations.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/donations",
    name: "Donations",
    component: Donations,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
