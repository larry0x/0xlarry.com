import { createStore } from "vuex";
import connects from "@/assets/connects.json";
import projects from "@/assets/projects.json";
import qrCodes from "@/assets/qrCodes.json";

export default createStore({
  state: { connects, projects, qrCodes },
});
